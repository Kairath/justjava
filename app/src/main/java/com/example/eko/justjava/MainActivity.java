package com.example.eko.justjava;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    int num = 0;
    int price = 3;

    public void submitOrder(View view) {
        String msg = createOrderSummary();
        composeEmail(msg, ordererName());
    }

    public void composeEmail(String msg, String name) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_TEXT, msg );
        intent.putExtra(Intent.EXTRA_SUBJECT, "order of the " + name);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void incOrder(View view) {
        if(num == 100) {
            Toast.makeText(this, "You cant have more than 100 coffee", Toast.LENGTH_SHORT ).show();
            return;
        }
        num += 1;
        display(num);
    }

    public void decOrder(View view) {
        if( num == 1 ){ return; }
        num -= 1;
        display(num);
    }

    /**
     * This method displays the given quantity value on the screen.
     */
    private void display(int number) {
        TextView quantityTextView = (TextView) findViewById(
                R.id.quantity_text_view);
        quantityTextView.setText("" + number);
    }

    private String createOrderSummary() {
        CheckBox checking = (CheckBox) findViewById(
                R.id.cream_checkbox);
        String message = "Orderer's name: " + ordererName();
        message += "\n" + getString(R.string.quantity) + ":" + num;
        message += "\nTotal: " + NumberFormat.getCurrencyInstance().format(num * price);
        message += "\n" + getString(R.string.thank_you);
        if(checking.isChecked()) { message += "\n" + getString(R.string.checked); }
        return message;
    }

    private String ordererName(){
        EditText nameField = (EditText) findViewById(R.id.name_field);
        String nameOfOrderer = nameField.getText().toString();
        return nameOfOrderer;
    }


}
